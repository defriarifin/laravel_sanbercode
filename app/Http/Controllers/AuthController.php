<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome(Request $request) {
        // dd($request->all());
        $data = $request->all();
        $fullname = ucfirst( $data ["fullname"]);
        $lastname = ucfirst( $data ["lastname"]);

        return view ('welcome', compact('fullname','lastname'));
    }
    
    public function register() {
        return view('register');
    }
}